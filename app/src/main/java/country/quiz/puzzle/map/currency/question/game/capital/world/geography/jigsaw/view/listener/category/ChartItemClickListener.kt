package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.listener.category

import android.view.View

interface ChartItemClickListener {
    fun onChartClickListener(view: View, position: Int)
}