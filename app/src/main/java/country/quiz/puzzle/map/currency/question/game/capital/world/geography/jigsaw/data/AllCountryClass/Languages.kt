package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.data.AllCountryClass

import com.google.gson.annotations.SerializedName

data class Languages(
	@SerializedName("name") val name: String? = null
)
