package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.data

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class DataFactory {
//    HOME

    val URL_ALL_APPS = BASE_URL + "1FWSgjbRXtaT_batfkiU2P4xBcIi9o-MpRjldxvLI_-U/values/Sheet1!A2:E/"
    val URL_CAROUSEL_IMAGES = BASE_URL + "1AFbXhOp_Lax8KJeAnW9LMkGxybrRhNGyqPIUVrY8ORU/values/Sheet1!A2:E/"
    val URL_TOP_INTERNATIONAL = BASE_URL + "1mjvyFhJ50639BiecWE175bKq_dwTK_H-g6haNMdYeK0/values/Sheet1!A2:E/"
    val URL_HISTORY = BASE_URL + "18iV3HALmo0SDbcd1nmOfI6iEj7JYScMwjyw44JP4TCU/values/Sheet1!A2:D/"
    val URL_WORLD_TOUR = BASE_URL + "11Z1grCFdG68unFf11e3NB_QklEzVJ_1-0up_rw5jm-g/values/Sheet1!A2:E/"
    val URL_MOST_USEFUL_APPS = BASE_URL + "18I8LB50pREA7ivnxqM8TxxoN-TuPEmd3YzWWV8rLnjA/values/Sheet1!A2:E/"
    val URL_CONTINENTAL_CARD = BASE_URL + "1tp9ke_1ZmiLmtk1QEo8zXUAtmqO904sWqUpqJMYAQ5s/values/Sheet1!A2:D/"

//    val URL_HISTORY = BASE_URL + "1wcUzdb8NZjJNErZ5rzTgPt0E-J69vmLyy0H5XGpilBA/values/Sheet1!A2:E/"
//    val URL_WORLD_TOUR = BASE_URL + "1tp9ke_1ZmiLmtk1QEo8zXUAtmqO904sWqUpqJMYAQ5s/values/Sheet1!A2:E/"
//    val URL_MOST_USEFUL_APPS = BASE_URL + "1FWSgjbRXtaT_batfkiU2P4xBcIi9o-MpRjldxvLI_-U/values/Sheet1!A2:E/"



    val URL_CARD_START = BASE_URL
    val URL_CARD_END = "/values/Sheet1!A2:E/"

    val KEY = "AIzaSyB21bRVxi8IVsINBqyRDKwq3b6bWr4gRys"

    companion object{
        val detailsInterface: DetailsInterface

        init {
            val BASE_URL = "https://restcountries.eu/rest/v2/"
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            detailsInterface = retrofit.create(DetailsInterface::class.java)
        }


        private val BASE_URL = "https://sheets.googleapis.com/v4/spreadsheets/"

        fun create(): DataService? {
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
            return retrofit.create(DataService::class.java)
        }
    }

//    companion object DetailsService {
//        val detailsInterface: DetailsInterface
//
//        init {
//            val BASE_URL = "https://restcountries.eu/rest/v2/"
//            val retrofit = Retrofit.Builder()
//                .baseUrl(BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build()
//            detailsInterface = retrofit.create(DetailsInterface::class.java)
//        }
//    }
}
