package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.adapter

import android.content.Context
import android.view.ViewGroup
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.base.adapter.GenericRecyclerAdapter
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.listener.CategoryStoresItemClickListener
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.viewholder.CategoryStoresViewHolder

class CategoryStoresAdapter(context: Context?) :
    GenericRecyclerAdapter<List<String>, CategoryStoresItemClickListener<List<String>>, CategoryStoresViewHolder>(context) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryStoresViewHolder {
        return CategoryStoresViewHolder(inflate(R.layout.card_all_apps_portal_layout,parent))
    }
}