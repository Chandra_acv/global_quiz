package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.viewpager
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.model.AllAppsModel
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.fragment.*

class AppPagerAdapter(fragmentManager: FragmentManager,val homeFragmentData: AllAppsModel?) : FragmentPagerAdapter(fragmentManager) {

    val NUM_ITEMS = 4;

    override fun getCount(): Int {
        return NUM_ITEMS
    }

    override fun getItem(position: Int): Fragment {
        when (position){
            0 -> {
                return MapsFragment.newInstance(position, "Continent")
            }
            1 -> {
                Log.d("HOme", homeFragmentData.toString())
                return CountryFragment.newInstance(position, "Home",homeFragmentData!!)
            }
            2 -> {
                return Quiz1Fragment.newInstance(position, "Quiz 1")
            }
            3 -> {
                return WebsiteCategoriesFragment.newInstance(position, "Quiz 2")
            }

            else -> return CountryFragment.newInstance(position, "Home",homeFragmentData!!)
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title : String? = null;
        if (position == 0) {
            title = "Map"
        } else if (position == 1) {
            title = "Country"
        }
        else if (position == 2) {
            title = "Quiz 1"
        }
        else if (position == 3) {
            title = "Quiz 2"
        }
        return title
    }
}