package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.ads.*
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.synnapps.carouselview.CarouselView
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.base.BaseFragment
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.data.AllCountryClass.CountryAllDetails
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.data.DataFactory
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.model.AllAppsModel
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.utils.Constants
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.MainActivity
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.WebActivity
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.adapter.DetailsAdapter
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Response
import java.io.FileNotFoundException
import java.io.IOException
import java.io.ObjectInputStream

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val ARG_PARAM3 = "param3"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentHome.newInstance] factory method to
 * create an instance of this fragment.
 */
class CountryFragment : BaseFragment(){
    private var param1: Int? = null
    private var param2: String? = null

    var carouselView: CarouselView? = null

    var rvAllApps: RecyclerView? = null
//    var allAppsAdapter: AllAppsAdapter? = null
    var homeViewModel: HomeViewModel? = null

//    var historyAdapter: HistoryAdapter? = null
//    var worldTourAdapter: WorldTourAdapter? = null

    var firebaseRemoteConfig: FirebaseRemoteConfig? = null
    var firebaseAnalytics: FirebaseAnalytics? = null
    var homeData: AllAppsModel? = null
    var historyList: ArrayList<List<String>>? = ArrayList()
    var worldTourList: ArrayList<List<String>>? = ArrayList()

    private var nativeAdFB1: NativeAd? = null
    private var nativeAdFB2: NativeAd? = null
    private var nativeAdLayout: NativeAdLayout? = null
    private var adView: LinearLayout? = null

    override val bindingVariable: Int
        get() = 0
    override val layoutId: Int
        get() = 0


    lateinit var recyclerView: RecyclerView
    lateinit var adapter: DetailsAdapter
    lateinit var searchView: EditText
    lateinit var search: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getInt(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
//            homeData = arguments!!.getSerializable(ARG_PARAM3) as AllAppsModel
//            Log.d("Home", homeData.toString())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_country, container, false)
        recyclerView = view.findViewById(R.id.recyclerView)
        searchView = view.findViewById(R.id.searchView)
        getDetails()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews(view)

        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance()

        homeData = read(context!!, "home.json")
//        setRecyclerView()

        homeViewModel = ViewModelProvider(activity!!).get(HomeViewModel::class.java)

        homeViewModel?.loadData()

//        homeViewModel!!.allAppsLiveData.observe(this, Observer { t ->
//            Log.d("TAG", "HomeFragment Live allAppsLiveData$t")
//            allAppsAdapter.setItems(t)
//        })

        homeViewModel!!.historyLiveData.observe(this, Observer { t ->
            Log.d("TAG", "HomeFragment Live History $t")
            historyList!!.addAll(t!!)
        })

        homeViewModel!!.worldTourLiveData.observe(this, Observer { t ->
            Log.d("TAG", "HomeFragment Live History $t")
            worldTourList!!.addAll(t!!)
        })

//        homeViewModel!!.topInternationalLiveData.observe(this, Observer { t ->
//            Log.d("TAG", "HomeFragment Live topInternationalLiveData $t")
//            topInternationalAdapter!!.setItems(t)
//        })

        if (firebaseRemoteConfig!!.getBoolean(Constants().SHOW_ADS)) {
            onLoadFBNativeAd1(view!!, context!!)
            onLoadFBNativeAd2(view!!, context!!)

        }


    }

    private fun getDetails()
    {
        val country = DataFactory.detailsInterface.getAllCountry()
        country.enqueue(object : retrofit2.Callback<ArrayList<CountryAllDetails>> {
            override fun onResponse(call: Call<ArrayList<CountryAllDetails>>, response: Response<ArrayList<CountryAllDetails>>
            ) {
                val detail = response.body()
                if (detail != null) {
                    Log.d(" response ", detail.get(0).name.toString())
                    adapter = DetailsAdapter(context ?: return, detail)
                    recyclerView.adapter = adapter
                    recyclerView.layoutManager = LinearLayoutManager(context)

                    searchView.addTextChangedListener(object : TextWatcher {
                        override fun beforeTextChanged(
                            s: CharSequence?,
                            start: Int,
                            count: Int,
                            after: Int
                        ) {

                        }

                        override fun onTextChanged(
                            s: CharSequence?,
                            start: Int,
                            before: Int,
                            count: Int
                        ) {

                        }

                        override fun afterTextChanged(s: Editable?) {
                            filter(s.toString())
                        }

                        private fun filter(text: String) {
                            var filteredList: ArrayList<CountryAllDetails>? = ArrayList()
                            for (item in detail) {
                                if (item.name?.toLowerCase()?.contains(text.toLowerCase()) == true) {
                                    filteredList?.add(item)
                                }
                            }
                            if (filteredList != null) {
                                adapter.filterList(filteredList)
                            }
                        }
                    })
                }
            }

            override fun onFailure(call: Call<ArrayList<CountryAllDetails>>, t: Throwable) {
                Log.e("Error", t.toString())
            }
        })
    }

    fun initViews(view: View) {
        firebaseAnalytics = FirebaseAnalytics.getInstance(activity!!)
        carouselView = view.findViewById(R.id.cvHome)
        rvAllApps = view.findViewById(R.id.rvAllApps)
    }

    fun read(context: Context, fileName: String): AllAppsModel? {
        return try {
            val fis = context.openFileInput(fileName)
            val `is` = ObjectInputStream(fis)
            val allAppsModel: AllAppsModel = `is`.readObject() as AllAppsModel
            `is`.close()
            fis.close()
            return allAppsModel
        } catch (fileNotFound: FileNotFoundException) {
            null
        } catch (ioException: IOException) {
            null
        }
    }

    fun onLoadFBNativeAd1(view: View, context: Context) {
        nativeAdFB1 = NativeAd(context, Constants().getFbNativeHome1())
        val nativeAdListener: NativeAdListener = object : NativeAdListener {
            override fun onError(p0: Ad?, p1: AdError?) {
                Log.d("TAG", "onError: onLoadFBNativeAd1 " + p1!!.errorMessage)
            }

            override fun onAdLoaded(ad: Ad?) {

                // Race condition, load() called again before last ad was displayed
                if (nativeAdFB1 == null || nativeAdFB1 !== ad) {
                    return
                }
                // Inflate Native Ad into Container

                // Add the Ad view into the ad container.
                nativeAdLayout = view.findViewById(R.id.native_ad_container_home_1)
                val inflater = LayoutInflater.from(context)
                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                adView =
                    inflater.inflate(
                        R.layout.native_ad_layout,
                        nativeAdLayout,
                        false
                    ) as LinearLayout
                nativeAdLayout!!.addView(adView)

                inflateAd(nativeAdFB1!!, adView!!)

                val adChoicesContainer: LinearLayout = view.findViewById(R.id.ad_choices_container)
                val adOptionsView = AdOptionsView(context, nativeAdFB1, nativeAdLayout)
                adChoicesContainer.removeAllViews()
                adChoicesContainer.addView(adOptionsView, 0)
            }

            override fun onAdClicked(p0: Ad?) {
                Log.d("TAG", "onAdClicked: onLoadFBNativeAd1")
            }

            override fun onLoggingImpression(p0: Ad?) {
                Log.d("TAG", "onLoggingImpression: onLoadFBNativeAd1")
            }

            override fun onMediaDownloaded(p0: Ad?) {
                Log.d("TAG", "onMediaDownloaded: onLoadFBNativeAd1")
            }
        }

        nativeAdFB1!!.loadAd(
            nativeAdFB1!!.buildLoadAdConfig()
                .withAdListener(nativeAdListener)
                .build()
        );
    }

    fun onLoadFBNativeAd2(view: View, context: Context) {
        nativeAdFB2 = NativeAd(context, Constants().getFbNativeHome2())
        val nativeAdListener: NativeAdListener = object : NativeAdListener {
            override fun onError(p0: Ad?, p1: AdError?) {
                Log.d("TAG", "onError: onLoadFBNativeAd1 " + p1!!.errorMessage)
            }

            override fun onAdLoaded(ad: Ad?) {

                // Race condition, load() called again before last ad was displayed
                if (nativeAdFB2 == null || nativeAdFB2 !== ad) {
                    return
                }
                // Inflate Native Ad into Container

                // Add the Ad view into the ad container.
                nativeAdLayout = view.findViewById(R.id.native_ad_container_home_2)
                val inflater = LayoutInflater.from(context)
                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                adView =
                    inflater.inflate(
                        R.layout.native_ad_layout,
                        nativeAdLayout,
                        false
                    ) as LinearLayout
                nativeAdLayout!!.addView(adView)

                inflateAd(nativeAdFB2!!, adView!!)

                val adChoicesContainer: LinearLayout = view.findViewById(R.id.ad_choices_container)
                val adOptionsView = AdOptionsView(context, nativeAdFB2, nativeAdLayout)
                adChoicesContainer.removeAllViews()
                adChoicesContainer.addView(adOptionsView, 0)
            }

            override fun onAdClicked(p0: Ad?) {
                Log.d("TAG", "onAdClicked: onLoadFBNativeAd1")
            }

            override fun onLoggingImpression(p0: Ad?) {
                Log.d("TAG", "onLoggingImpression: onLoadFBNativeAd1")
            }

            override fun onMediaDownloaded(p0: Ad?) {
                Log.d("TAG", "onMediaDownloaded: onLoadFBNativeAd1")
            }
        }

        nativeAdFB2!!.loadAd(
            nativeAdFB2!!.buildLoadAdConfig()
                .withAdListener(nativeAdListener)
                .build()
        );
    }


    private fun inflateAd(nativeAd: NativeAd, adView: LinearLayout) {
        nativeAd.unregisterView()

        // Add the AdOptionsView

        // Create native UI using the ad metadata.
        val nativeAdIcon: com.facebook.ads.MediaView = adView.findViewById(R.id.native_ad_icon)
        val nativeAdTitle: TextView = adView.findViewById(R.id.native_ad_title)
        val nativeAdMedia: com.facebook.ads.MediaView = adView.findViewById(R.id.native_ad_media)
        val nativeAdSocialContext: TextView = adView.findViewById(R.id.native_ad_social_context)
        val nativeAdBody: TextView = adView.findViewById(R.id.native_ad_body)
        val sponsoredLabel: TextView = adView.findViewById(R.id.native_ad_sponsored_label)
        val nativeAdCallToAction: Button = adView.findViewById(R.id.native_ad_call_to_action)

        // Set the Text.
        nativeAdTitle.text = nativeAd.advertiserName
        nativeAdBody.text = nativeAd.adBodyText
        nativeAdSocialContext.text = nativeAd.adSocialContext
        nativeAdCallToAction.visibility =
            if (nativeAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE
        nativeAdCallToAction.text = nativeAd.adCallToAction
        sponsoredLabel.text = nativeAd.sponsoredTranslation

        // Create a list of clickable views
        val clickableViews: ArrayList<View> = ArrayList()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
            adView, nativeAdMedia, nativeAdIcon, clickableViews
        )
    }


    override fun onDestroy() {
        homeViewModel?.reset()
        Log.d("TAG", "onDestroy: ")
        super.onDestroy()
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentHome.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: Int, param2: String, allAppsModel: AllAppsModel) =
            CountryFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

}
