package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.Singleton
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.data.DataFactory
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.data.DataService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

class CategoryViewModel : ViewModel() {
    var historyLiveData: MutableLiveData<List<List<String>>?> = MutableLiveData()
    var worldTourLiveData: MutableLiveData<List<List<String>>?> = MutableLiveData()
    var mostUsefulAppsLiveData: MutableLiveData<List<List<String>>?> = MutableLiveData()

    private var context: Context? = null
    var compositeDisposable: CompositeDisposable? = null

    fun loadData(){
        Log.d("TAG", "loadData: ")
        compositeDisposable = CompositeDisposable()
        fetchHistory()
        fetchWorldTour()
        fetchMostUsefulApps()
    }

    private fun fetchHistory(){
        Log.d("TAG", "fetchHistory: ")
        val singleton: Singleton? = Singleton.get()
        val dataService: DataService? = singleton!!.getDataService()


        val disposable: Disposable?
        disposable = dataService?.fetchAllApps(DataFactory().URL_HISTORY, DataFactory().KEY)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnError(Consumer { t ->
                Log.d("TAG", "fetchHistory Error ${t.localizedMessage}")
            })
            ?.subscribe(Consumer { t ->
                Log.d("TAG", "fetchHistory Response ${t.getValues()}")
                changeHistoryDataSet(t.getValues())
            })

        if (disposable != null) {
            compositeDisposable?.add(disposable)
        }
    }

    private fun fetchWorldTour(){
        Log.d("TAG", "fetchWorldTour: ")
        val singleton: Singleton? = Singleton.get()
        val dataService: DataService? = singleton!!.getDataService()


        val disposable: Disposable?
        disposable = dataService?.fetchAllApps(DataFactory().URL_WORLD_TOUR, DataFactory().KEY)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnError(Consumer { t ->
                Log.d("TAG", "fetchWorldTour Error ${t.localizedMessage}")
            })
            ?.subscribe(Consumer { t ->
                Log.d("TAG", "fetchWorldTour Response ${t.getValues()}")
                changeWorldTourDataSet(t.getValues())
            })

        if (disposable != null) {
            compositeDisposable?.add(disposable)
        }
    }

    private fun fetchMostUsefulApps(){
        Log.d("TAG", "fetchMostUsefulApps: ")
        val singleton: Singleton? = Singleton.get()
        val dataService: DataService? = singleton!!.getDataService()


        val disposable: Disposable?
        disposable = dataService?.fetchAllApps(DataFactory().URL_MOST_USEFUL_APPS, DataFactory().KEY)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnError(Consumer { t ->
                Log.d("TAG", "fetchMostUsefulApps Error ${t.localizedMessage}")
            })
            ?.subscribe(Consumer { t ->
                Log.d("TAG", "fetchMostUsefulApps Response ${t.getValues()}")
                changeMostUsefulAppsDataSet(t.getValues())
            })

        if (disposable != null) {
            compositeDisposable?.add(disposable)
        }
    }

    fun changeHistoryDataSet(allAppsList: List<List<String>>?){
        historyLiveData.value = allAppsList
    }

    fun changeWorldTourDataSet(allAppsList: List<List<String>>?){
        mostUsefulAppsLiveData.value = allAppsList
    }

    fun changeMostUsefulAppsDataSet(allAppsList: List<List<String>>?){
        mostUsefulAppsLiveData.value = allAppsList
    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable!!.isDisposed) {
            compositeDisposable!!.dispose()
        }
    }

    fun reset() {
        unSubscribeFromObservable()
        compositeDisposable = null
        context = null
    }
}