package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.ActivityConstants
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R

class Round2Activity : AppCompatActivity() {
    private val KEY_NAME = "sec2_url"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_round2)

        val round1_score = intent.getIntExtra("score",0)
        val score_TextView = findViewById<TextView>(R.id.score_txt)
        score_TextView.text = "Score: $round1_score / 10"

        val btn_round2 = findViewById<Button>(R.id.round2Btn)
        btn_round2.setOnClickListener{
            Toast.makeText(this, "Quiz started", Toast.LENGTH_LONG).show()
            val intent = Intent(applicationContext, LoadingActivity::class.java)
            intent.putExtra("score",round1_score)
            intent.putExtra("name" , ActivityConstants.ROUND2_ACTIVITY)
            startActivity(intent)
            finish()
        }
    }
}