package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.data.AllCountryClass

import com.google.gson.annotations.SerializedName

data class Currencies(
	@SerializedName("name") val name: String? = null,
	@SerializedName("symbol") val symbol: String? = null
)
