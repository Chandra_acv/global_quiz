package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.core.app.ActivityCompat
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R

class ScoreActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score)

        val total_Score = intent.getIntExtra("score",0)
        val score_TextView = findViewById<TextView>(R.id.tot_score_txt)
        score_TextView.text = total_Score.toString()

        val playAgain_Btn = findViewById<Button>(R.id.play_again_btn)
        playAgain_Btn.setOnClickListener{
            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        val exitApp_Btn = findViewById<Button>(R.id.exit_btn)
        exitApp_Btn.setOnClickListener{
            ActivityCompat.finishAffinity(this)
        }
    }
}