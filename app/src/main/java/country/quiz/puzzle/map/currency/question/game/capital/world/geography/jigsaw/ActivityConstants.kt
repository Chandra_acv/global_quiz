package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw

interface ActivityConstants {
        companion object {
            const val MAIN_ACTIVITY = 1001
            const val ROUND2_ACTIVITY = 1002
            const val LOADING_ACTIVITY = 1003
        }
}