package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.data

//import country.flages.capital.learning.map.world.guess.wallpapers.countryapp.Data.AllCountryClass.CountryAllDetails
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.data.AllCountryClass.CountryAllDetails
import retrofit2.Call
import retrofit2.http.GET

interface DetailsInterface {
    @GET("all")
    fun getAllCountry(): Call<ArrayList<CountryAllDetails>>

}



