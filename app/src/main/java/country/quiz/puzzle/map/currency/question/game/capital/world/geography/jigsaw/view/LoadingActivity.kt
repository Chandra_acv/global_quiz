package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.ActivityConstants
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class LoadingActivity : AppCompatActivity() {

    private val TAG = "LoadingActivity"
    private val KEY_NAME = "QuizQs"
    lateinit var QuizQuestions: ArrayList<QuestionAnswer>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)

        val activityName = intent.getIntExtra("name",0)

        if(activityName == ActivityConstants.MAIN_ACTIVITY)
        {
            APIRequest("https://opentdb.com/api.php?amount=10&category=22&difficulty=easy&type=multiple")
        }
        else if(activityName == ActivityConstants.ROUND2_ACTIVITY)
        {
            APIRequest("https://opentdb.com/api.php?amount=10&category=22&difficulty=medium&type=multiple")
        }

    }

    private fun APIRequest(url:String)
    {
        val reqQueue = Volley.newRequestQueue(this)
        val objReq = JsonObjectRequest(
            Request.Method.GET, url, null,
            Response.Listener<JSONObject>
            { response ->
                Log.d(TAG, "Response got successfully!")

                var JsonArr: JSONArray?= try { response.getJSONArray("results") } catch (e: JSONException) {null}
                Log.d(TAG, JsonArr?.length().toString())
                //textView.text = "Response: %s".format(response.toString())
                QuizQuestions = extractQuestions(JsonArr)

                val score = intent.getIntExtra("score",0)
                val intent = Intent(applicationContext, QuizActivity::class.java)
                intent.putExtra(KEY_NAME,QuizQuestions)
                intent.putExtra("score",score)
                startActivity(intent)
                finish()
            },
            Response.ErrorListener { error ->
                Log.d(TAG, "Something went wrong! Error in JSON Response")
                error.printStackTrace()

            })

        reqQueue.add(objReq)
    }

    private fun extractQuestions(JsonArr: JSONArray?): ArrayList<QuestionAnswer>{
        QuizQuestions = arrayListOf()
        for(i in 0..(JsonArr?.length()!!-1))
        {
            val Q1: JSONObject = JsonArr?.get(i) as JSONObject
            //Log.d("Iterate", i.toString())
            val inc_ans= Q1.getJSONArray("incorrect_answers")

//            Log.d(TAG,Q1.getString("question"))
//            Log.d(TAG, inc_ans.toString())
//            Log.d(TAG,Q1.getString("correct_answer"))

            var correctAns_index = (0..(inc_ans.length())).random()
            //Iterating over each item in JSONArray and saving into Array<String> to send it to the QuestionAnswer constructor
            var options: ArrayList<String> = arrayListOf<String>()
            var k = 0
            for(j in 0..inc_ans.length())
            {
                if(j!=correctAns_index)
                {
                    options.add(inc_ans[k].toString())
                    k++
                }
                else
                    options.add(Q1.getString("correct_answer"))
            }
            QuizQuestions.add(QuestionAnswer(Q1.getString("question"),
                Q1.getString("correct_answer"),
                options
            ))
        }
        return QuizQuestions
    }
}