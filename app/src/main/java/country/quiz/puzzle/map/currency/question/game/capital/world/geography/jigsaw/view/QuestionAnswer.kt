package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view

import java.io.Serializable

class QuestionAnswer(var Q: String, var ans: String,var opts: ArrayList<String>):Serializable {
    fun isCorrect(chosen: String): Boolean
    {
        return if(chosen.equals(ans)) { true } else { false }
    }
}