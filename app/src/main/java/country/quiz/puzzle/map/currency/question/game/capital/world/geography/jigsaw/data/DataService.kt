package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.data

import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.model.AllAppsModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface DataService {
    @GET
    fun fetchAllApps(@Url url: String, @Query("key") key: String): Observable<AllAppsModel>

}