package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import android.widget.*
import com.google.android.material.radiobutton.MaterialRadioButton
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R

class QuizActivity : AppCompatActivity() {

    private var QuestionNo = 0
    private var correct_count = 0
    private var isRound2Over = false
    private fun openCorrectDialog(d:Dialog) {
        d.setContentView(R.layout.correct_dialog_layout)
        d.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        d.show()
        //d.dismiss()
    }

    private fun openWrongDialog(d:Dialog) {
        d.setContentView(R.layout.wrong_dialog_layout)
        d.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        d.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)

        var QuizQs = intent.getSerializableExtra("QuizQs") as ArrayList<QuestionAnswer>


        var Qs = QuizQs.get(QuestionNo)

        createDynamicRadioButtons(Qs.Q,Qs.opts[0],Qs.opts[1],Qs.opts[2],Qs.opts[3])

        val radioGroup = findViewById<RadioGroup>(R.id.radioGrp)
        radioGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { radioGroup, i ->
            //Toast.makeText(this,"option "+i+" is selected",Toast.LENGTH_LONG).show()
            val selectedOption: Int = radioGroup!!.checkedRadioButtonId

            var option = findViewById<RadioButton>(selectedOption)

            //Toast.makeText(this, option.text, Toast.LENGTH_SHORT).show()
            val dialog = Dialog(this)
            if(Qs.isCorrect(option.text.toString()))
            {
                correct_count++
                option.setBackgroundColor(Color.GREEN)
                //Toast.makeText(this, "CORRECT ANS", Toast.LENGTH_SHORT).show()
                openCorrectDialog(dialog)
            }
            else
            {
                openWrongDialog(dialog)
            }
            QuestionNo = QuestionNo+1

            if(QuestionNo>=10)
            {
                var score = intent.getIntExtra("score",0)
                if(score == 0)
                {
                    dialog.dismiss()
                    val intent = Intent(this, Round2Activity::class.java)
                    intent.putExtra("score",correct_count)
                    startActivity(intent)
                    finish()
                }
                else
                {
                    dialog.dismiss()
                    score += correct_count
                    val intent = Intent(this, ScoreActivity::class.java)
                    intent.putExtra("score",score)
                    startActivity(intent)
                    finish()
                }
            }
            else
            {
                Qs = QuizQs.get(QuestionNo)
                createDynamicRadioButtons(Qs.Q,Qs.opts[0],Qs.opts[1],Qs.opts[2],Qs.opts[3])
            }

        })
    }

    @SuppressLint("ResourceType")
    public fun createDynamicRadioButtons(question: String, btn1_txt:String, btn2_txt:String, btn3_txt:String, btn4_txt:String)
    {
        val radioButton1 = MaterialRadioButton(this,null, R.attr.materialButtonOutlinedStyle)
        radioButton1.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        radioButton1.setText(btn1_txt)
        radioButton1.id = 0

        val radioButton2 = MaterialRadioButton(this,null, R.attr.materialButtonOutlinedStyle)
        radioButton2.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        radioButton2.setText(btn2_txt)
        radioButton2.id = 1

        val radioButton3 = MaterialRadioButton(this,null, R.attr.materialButtonOutlinedStyle)
        radioButton3.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        radioButton3.setText(btn3_txt)
        radioButton3.id = 2

        val radioButton4 = MaterialRadioButton(this,null, R.attr.materialButtonOutlinedStyle)
        radioButton4.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        radioButton4.setText(btn4_txt)
        radioButton4.id = 3

        var q_text = findViewById<TextView>(R.id.question)
        q_text.text = question

        val radioGroup = findViewById<RadioGroup>(R.id.radioGrp)
        radioGroup.removeAllViews()
        if (radioGroup != null) {
            radioGroup.addView(radioButton1)
            radioGroup.addView(radioButton2)
            radioGroup.addView(radioButton3)
            radioGroup.addView(radioButton4)
        }
    }
}