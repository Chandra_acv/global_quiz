package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.fragment

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.ads.*
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.ActivityConstants
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.base.BaseFragment
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.utils.Constants
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.MainActivity
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.WebActivity
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.LoadingActivity
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.adapter.CategoryStoresAdapter
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.listener.CategoryStoresItemClickListener
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.viewmodel.CategoryViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Quiz1Fragment : BaseFragment(), CategoryStoresItemClickListener<List<String>> {
    // TODO: Rename and change types of parameters
    private var param1: Int? = null
    private var param2: String? = null

    var rvCategoryStores: RecyclerView? = null
    var categoryStoresAdapter: CategoryStoresAdapter? = null
    var categoryViewModel: CategoryViewModel? = null

    var firebaseRemoteConfig: FirebaseRemoteConfig? = null
    var firebaseAnalytics: FirebaseAnalytics? = null
    private var nativeAdFB1: NativeAd? = null
    private var nativeAdFB2: NativeAd? = null
    private var nativeAdFB3: NativeAd? = null
    private var nativeAdFB4: NativeAd? = null

    private var nativeAdLayout: NativeAdLayout? = null
    private var adView: LinearLayout? = null
    var llHistory: LinearLayout? = null
    var llWorldTour: LinearLayout? = null
    var llMostUsefulApps: LinearLayout? = null

    private val KEY_NAME = "start_url"
    private val TAG = "QuizStartFragment"
    private var button: Button ? = null

    var historyList: ArrayList<List<String>>? = ArrayList()
    var worldTourList: ArrayList<List<String>>? = ArrayList()
    var mostUsefulAppsList: ArrayList<List<String>>? = ArrayList()

    var dialog: Dialog? = null

    override val bindingVariable: Int
        get() = 0
    override val layoutId: Int
        get() = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getInt(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.layout.fragment_quiz_start, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews(view)

        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance()

        dialog = Dialog(context!!)

        categoryViewModel = ViewModelProvider(activity!!).get(CategoryViewModel::class.java)
        categoryViewModel?.loadData()

        categoryViewModel!!.historyLiveData.observe(this, Observer { t ->
            Log.d("TAG", "onViewCreated: historyLiveData $t")
            historyList!!.addAll(t!!)
        })

        categoryViewModel!!.worldTourLiveData.observe(this, Observer { t ->
            Log.d("TAG", "onViewCreated: worldTourLiveData $t")
            worldTourList!!.addAll(t!!)
        })

        categoryViewModel!!.mostUsefulAppsLiveData.observe(this, Observer { t ->
            Log.d("TAG", "onViewCreated: mostUsefulAppsLiveData $t")
            mostUsefulAppsList!!.addAll(t!!)
        })

        llHistory!!.setOnClickListener {
            onShowStores(historyList!!, view)
        }

        llWorldTour!!.setOnClickListener {
            onShowStores(worldTourList!!, view)
        }

        llMostUsefulApps!!.setOnClickListener {
            onShowStores(mostUsefulAppsList!!, view)
        }

        if (firebaseRemoteConfig!!.getBoolean(Constants().SHOW_ADS)) {
            onLoadFBNativeAdCatDailog(view, context!!, dialog!!)
        }

        button?.setOnClickListener()
        {
            // displaying a toast message
            //Toast.makeText(this@MainActivity, "Quiz started", Toast.LENGTH_LONG).show()
            val intent = Intent(activity, LoadingActivity::class.java)
            intent.putExtra("score",0)
            intent.putExtra("name" , ActivityConstants.MAIN_ACTIVITY)
            startActivity(intent)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        if(isVisibleToUser){
            if(firebaseRemoteConfig == null){
                firebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
            }
            if (firebaseRemoteConfig!!.getBoolean(Constants().SHOW_ADS)) {
                onLoadFBNativeAd1(view!!, context!!)
                onLoadFBNativeAd2(view!!, context!!)
                onLoadFBNativeAd3(view!!, context!!)

            }
        }

    }

    fun initViews(view: View){
        firebaseAnalytics = FirebaseAnalytics.getInstance(activity!!)
        button = view.findViewById<Button>(R.id.btn1)
        llHistory = view.findViewById(R.id.llHistory)
        llWorldTour = view.findViewById(R.id.llWorldTour)
        llMostUsefulApps = view.findViewById(R.id.llMostUsefulApps)
    }

    fun setRecyclerView(){
        categoryStoresAdapter = CategoryStoresAdapter(context)
        categoryStoresAdapter!!.setListener(this)
        rvCategoryStores.apply {
            rvCategoryStores?.layoutManager = GridLayoutManager(activity, 3)
            rvCategoryStores?.adapter = categoryStoresAdapter
        }
    }

    fun onShowStores(list: ArrayList<List<String>>, view: View){
        dialog!!.setContentView(R.layout.dialog_show_stores)
        dialog!!.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT);
        rvCategoryStores = dialog!!.findViewById(R.id.rvCategoryStores)
        setRecyclerView()
        categoryStoresAdapter!!.setItems(list)
        if(firebaseRemoteConfig!!.getBoolean(Constants().SHOW_ADS)){
            onLoadFBNativeAdCatDailog(view, context!!, dialog!!)
        }

        dialog!!.show()

    }

    fun onLoadFBNativeAd1(view: View, context: Context) {
        nativeAdFB1 = NativeAd(context, Constants().getFbNativeCat1())
        val nativeAdListener: NativeAdListener = object : NativeAdListener {
            override fun onError(p0: Ad?, p1: AdError?) {
                Log.d("TAG", "onError: onLoadFBNativeAd1 " + p1!!.errorMessage)
            }

            override fun onAdLoaded(ad: Ad?) {

                // Race condition, load() called again before last ad was displayed
                if (nativeAdFB1 == null || nativeAdFB1 !== ad) {
                    return
                }
                // Inflate Native Ad into Container

                // Add the Ad view into the ad container.
                nativeAdLayout = view.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.native_ad_container_cat_1)
                val inflater = LayoutInflater.from(context)
                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                adView =
                        inflater.inflate(
                            country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.layout.native_ad_layout,
                                nativeAdLayout,
                                false
                        ) as LinearLayout
                nativeAdLayout!!.addView(adView)

                inflateAd(nativeAdFB1!!, adView!!)

                val adChoicesContainer: LinearLayout = view.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.ad_choices_container)
                val adOptionsView = AdOptionsView(context, nativeAdFB1, nativeAdLayout)
                adChoicesContainer.removeAllViews()
                adChoicesContainer.addView(adOptionsView, 0)
            }

            override fun onAdClicked(p0: Ad?) {
                Log.d("TAG", "onAdClicked: onLoadFBNativeAd1")
            }

            override fun onLoggingImpression(p0: Ad?) {
                Log.d("TAG", "onLoggingImpression: onLoadFBNativeAd1")
            }

            override fun onMediaDownloaded(p0: Ad?) {
                Log.d("TAG", "onMediaDownloaded: onLoadFBNativeAd1")
            }
        }

        nativeAdFB1!!.loadAd(
                nativeAdFB1!!.buildLoadAdConfig()
                        .withAdListener(nativeAdListener)
                        .build()
        );
    }

    fun onLoadFBNativeAd2(view: View,context: Context) {
        nativeAdFB2 = NativeAd(context, Constants().getFbNativeCat2())
        val nativeAdListener: NativeAdListener = object : NativeAdListener {
            override fun onError(p0: Ad?, p1: AdError?) {
                Log.d("TAG", "onError: onLoadFBNativeAd1 " + p1!!.errorMessage)
            }

            override fun onAdLoaded(ad: Ad?) {

                // Race condition, load() called again before last ad was displayed
                if (nativeAdFB2 == null || nativeAdFB2 !== ad) {
                    return
                }
                // Inflate Native Ad into Container

                // Add the Ad view into the ad container.
                nativeAdLayout = view.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.native_ad_container_cat_2)
                val inflater = LayoutInflater.from(context)
                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                adView =
                        inflater.inflate(
                            country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.layout.native_ad_layout,
                                nativeAdLayout,
                                false
                        ) as LinearLayout
                nativeAdLayout!!.addView(adView)

                inflateAd(nativeAdFB2!!, adView!!)

                val adChoicesContainer: LinearLayout = view.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.ad_choices_container)
                val adOptionsView = AdOptionsView(context, nativeAdFB2, nativeAdLayout)
                adChoicesContainer.removeAllViews()
                adChoicesContainer.addView(adOptionsView, 0)
            }

            override fun onAdClicked(p0: Ad?) {
                Log.d("TAG", "onAdClicked: onLoadFBNativeAd1")
            }

            override fun onLoggingImpression(p0: Ad?) {
                Log.d("TAG", "onLoggingImpression: onLoadFBNativeAd1")
            }

            override fun onMediaDownloaded(p0: Ad?) {
                Log.d("TAG", "onMediaDownloaded: onLoadFBNativeAd1")
            }
        }

        nativeAdFB2!!.loadAd(
                nativeAdFB2!!.buildLoadAdConfig()
                        .withAdListener(nativeAdListener)
                        .build()
        );
    }

    fun onLoadFBNativeAd3(view: View,context: Context) {
        nativeAdFB3 = NativeAd(context, Constants().getFbNativeCat3())
        val nativeAdListener: NativeAdListener = object : NativeAdListener {
            override fun onError(p0: Ad?, p1: AdError?) {
                Log.d("TAG", "onError: onLoadFBNativeAd1 " + p1!!.errorMessage)
            }

            override fun onAdLoaded(ad: Ad?) {

                // Race condition, load() called again before last ad was displayed
                if (nativeAdFB3 == null || nativeAdFB3 !== ad) {
                    return
                }
                // Inflate Native Ad into Container

                // Add the Ad view into the ad container.
                nativeAdLayout = view.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.native_ad_container_cat_3)
                val inflater = LayoutInflater.from(context)
                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                adView =
                        inflater.inflate(
                            country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.layout.native_ad_layout,
                                nativeAdLayout,
                                false
                        ) as LinearLayout
                nativeAdLayout!!.addView(adView)

                inflateAd(nativeAdFB3!!, adView!!)

                val adChoicesContainer: LinearLayout = view.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.ad_choices_container)
                val adOptionsView = AdOptionsView(context, nativeAdFB3, nativeAdLayout)
                adChoicesContainer.removeAllViews()
                adChoicesContainer.addView(adOptionsView, 0)
            }

            override fun onAdClicked(p0: Ad?) {
                Log.d("TAG", "onAdClicked: onLoadFBNativeAd1")
            }

            override fun onLoggingImpression(p0: Ad?) {
                Log.d("TAG", "onLoggingImpression: onLoadFBNativeAd1")
            }

            override fun onMediaDownloaded(p0: Ad?) {
                Log.d("TAG", "onMediaDownloaded: onLoadFBNativeAd1")
            }
        }

        nativeAdFB3!!.loadAd(
                nativeAdFB3!!.buildLoadAdConfig()
                        .withAdListener(nativeAdListener)
                        .build()
        );
    }

    fun onLoadFBNativeAdCatDailog(view: View, context: Context, dialog: Dialog) {
        nativeAdFB4 = NativeAd(context, Constants().getFbNativeDailog())
        val nativeAdListener: NativeAdListener = object : NativeAdListener {
            override fun onError(p0: Ad?, p1: AdError?) {
                Log.d("TAG", "onError: onLoadFBNativeAd1 " + p1!!.errorMessage)
            }

            override fun onAdLoaded(ad: Ad?) {

                // Race condition, load() called again before last ad was displayed
                if (nativeAdFB4 == null || nativeAdFB4 !== ad) {
                    return
                }
                // Inflate Native Ad into Container

                // Add the Ad view into the ad container.
                nativeAdLayout = dialog.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.native_ad_container_topic_dailog)
                val inflater = LayoutInflater.from(context)
                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                adView =
                        inflater.inflate(
                            country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.layout.native_ad_layout,
                                nativeAdLayout,
                                false
                        ) as LinearLayout
                nativeAdLayout!!.addView(adView)

                inflateAd(nativeAdFB4!!, adView!!)

                val adChoicesContainer: LinearLayout = dialog.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.ad_choices_container)
                val adOptionsView = AdOptionsView(context, nativeAdFB4, nativeAdLayout)
                adChoicesContainer.removeAllViews()
                adChoicesContainer.addView(adOptionsView, 0)
            }

            override fun onAdClicked(p0: Ad?) {
                Log.d("TAG", "onAdClicked: onLoadFBNativeAd1")
            }

            override fun onLoggingImpression(p0: Ad?) {
                Log.d("TAG", "onLoggingImpression: onLoadFBNativeAd1")
            }

            override fun onMediaDownloaded(p0: Ad?) {
                Log.d("TAG", "onMediaDownloaded: onLoadFBNativeAd1")
            }
        }

        nativeAdFB4!!.loadAd(
                nativeAdFB4!!.buildLoadAdConfig()
                        .withAdListener(nativeAdListener)
                        .build()
        );
    }




    private fun inflateAd(nativeAd: NativeAd, adView: LinearLayout) {
        nativeAd.unregisterView()

        // Add the AdOptionsView

        // Create native UI using the ad metadata.
        val nativeAdIcon: com.facebook.ads.MediaView = adView.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.native_ad_icon)
        val nativeAdTitle: TextView = adView.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.native_ad_title)
        val nativeAdMedia: com.facebook.ads.MediaView = adView.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.native_ad_media)
        val nativeAdSocialContext: TextView = adView.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.native_ad_social_context)
        val nativeAdBody: TextView = adView.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.native_ad_body)
        val sponsoredLabel: TextView = adView.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.native_ad_sponsored_label)
        val nativeAdCallToAction: Button = adView.findViewById(country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R.id.native_ad_call_to_action)

        // Set the Text.
        nativeAdTitle.text = nativeAd.advertiserName
        nativeAdBody.text = nativeAd.adBodyText
        nativeAdSocialContext.text = nativeAd.adSocialContext
        nativeAdCallToAction.visibility =
                if (nativeAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE
        nativeAdCallToAction.text = nativeAd.adCallToAction
        sponsoredLabel.text = nativeAd.sponsoredTranslation

        // Create a list of clickable views
        val clickableViews: ArrayList<View> = ArrayList()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView, nativeAdMedia, nativeAdIcon, clickableViews
        )
    }

    override fun onDestroy() {
        categoryViewModel?.reset()
        super.onDestroy()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CategoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: Int, param2: String) =
            Quiz1Fragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun CategoryStoresCardClick(item: List<String>) {
        Log.d("TAG", "onAllBrokersCardClick: " + item.get(1))

        val bundle = Bundle()
        bundle.putString("title", item.get(1))
        bundle.putString("url", item.get(2))
        (activity as MainActivity?)!!.onUpdateLogEvent(bundle,"brokers_visited",true)

        val intent: Intent? = Intent(activity, WebActivity::class.java)
        intent?.putExtra("title", item.get(1))
        intent?.putExtra("url", item.get(2))
        startActivity(intent)
    }
}