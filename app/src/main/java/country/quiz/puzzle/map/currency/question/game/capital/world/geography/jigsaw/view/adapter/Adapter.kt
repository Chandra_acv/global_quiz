package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.WebActivity

//class Adapter(val listener: CategoryItemClickListener): RecyclerView.Adapter<Adapter.ViewHolder>(){ //val context: Context, val categories: List<>
class Adapter(): RecyclerView.Adapter<Adapter.ViewHolder>(){

    private val itemImages = intArrayOf(
        R.drawable.world_flag_card,
        R.drawable.country_map_card,
        R.drawable.capital_quiz_card,
        R.drawable.jigsaw_puzzle_card,
        R.drawable.currency_card,
        R.drawable.dictionary_card
    )
    private val itemTitles = arrayListOf<String>(
        "World Flag",
        "Country Map",
        "Capital Quiz",
        "Jigsaw Puzzle",
        "Currency",
        "Dictionary")
    //private val itemDescs = arrayListOf<String>("Description 1","Description 2","Description 3","Description 4","Description 5")
    private val itemURLs = arrayListOf<String>(
        "https://world-geography-games.com/en/flags_world.html",
        "https://world-geography-games.com/countries_world.html",
        "https://www.britannica.com/quiz/countries-and-capitals-quiz",
        "https://www.jigsawplanet.com/",
        "https://www.mathsisfun.com/money/currency.html#",
        "https://www.mathsisfun.com/definitions/index.html")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.custom_card_layout,parent,false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.titleTxt.text = itemTitles[position]
        //holder.descTxt.text = itemDescs[position]
        holder.imageView.setImageResource(itemImages[position])


        holder.itemView.setOnClickListener {v: View ->
            //Toast.makeText(v.context,"Clicked on the item ${position+1}",Toast.LENGTH_SHORT).show()
            val intent = Intent(holder.context, WebActivity::class.java)
            intent.putExtra("url",itemURLs[position])
            holder.context.startActivity(intent)
            
        }
    }

    override fun getItemCount(): Int {
        return itemTitles.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView
        var titleTxt: TextView
        //var descTxt: TextView
        val context: Context

        init {
            imageView = itemView.findViewById(R.id.icon_image)
            titleTxt = itemView.findViewById(R.id.titleTxt)
            //descTxt = itemView.findViewById(R.id.descTxt)
            context = itemView.context
//            itemView.setOnClickListener{
//                val position = absoluteAdapterPosition
//                val url = itemURLs[position]
//                listener.OnCategoryCardClick(position,url)
//            }
        }

    }
}
//
//interface CategoryItemClickListener {
//    fun OnCategoryCardClick(position:Int,url:String)
//}
