package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.adapter

import android.content.Context
import android.view.ViewGroup
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.base.adapter.GenericRecyclerAdapter
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.databinding.ActivityDetailsPageBinding.inflate
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.listener.MostUsefulAppsItemClickListener
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.viewholder.MostUsefulAppsViewHolder

class MostUsefulAppsAdapter(context: Context?) :
    GenericRecyclerAdapter<List<String>, MostUsefulAppsItemClickListener<List<String>>, MostUsefulAppsViewHolder>(context) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MostUsefulAppsViewHolder {
        return MostUsefulAppsViewHolder(inflate(R.layout.card_all_apps_portal_layout,parent))
    }
}