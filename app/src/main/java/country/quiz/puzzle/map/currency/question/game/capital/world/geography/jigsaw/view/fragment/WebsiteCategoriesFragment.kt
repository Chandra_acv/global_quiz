package country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.fragment

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.ads.*
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.utils.Constants
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.R
import country.quiz.puzzle.map.currency.question.game.capital.world.geography.jigsaw.view.adapter.Adapter
import kotlinx.android.synthetic.main.fragment_icons.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [InvestFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WebsiteCategoriesFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: Int? = null
    private var param2: String? = null


    private var layoutManager: RecyclerView.LayoutManager?= null
    private var adapter: RecyclerView.Adapter<Adapter.ViewHolder>?= null
    private var recycler_view: RecyclerView ?= null

    var firebaseAnalytics: FirebaseAnalytics? = null
    var firebaseRemoteConfig: FirebaseRemoteConfig? = null
    private var nativeAdFB1: NativeAd? = null
    private var nativeAdFB2: NativeAd? = null
    private var nativeAdLayout: NativeAdLayout? = null
    private var adView: LinearLayout? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getInt(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_icons, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews(view)
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        setRecyclerView()

    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        if(isVisibleToUser){
            if(firebaseRemoteConfig == null){
                firebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
            }
            if (firebaseRemoteConfig!!.getBoolean(Constants().SHOW_ADS)) {
                onLoadFBNativeAd1(view!!, context!!)
//                onLoadFBNativeAd2(view!!, context!!)
            }
        }

    }

    fun initViews(view: View){
        firebaseAnalytics = FirebaseAnalytics.getInstance(activity!!)
        recycler_view = view.findViewById(R.id.recycler_view)
    }

    fun setRecyclerView(){
        layoutManager = LinearLayoutManager(view?.context)
        recycler_view?.layoutManager = layoutManager

        adapter = Adapter()
        recycler_view?.adapter = adapter
    }
    fun onLoadFBNativeAd1(view: View, context: Context) {
        nativeAdFB1 = NativeAd(context, Constants().getFbNativeTool1())
        val nativeAdListener: NativeAdListener = object : NativeAdListener {
            override fun onError(p0: Ad?, p1: AdError?) {
                Log.d("TAG", "onError: onLoadFBNativeAd1 " + p1!!.errorMessage)
            }

            override fun onAdLoaded(ad: Ad?) {

                // Race condition, load() called again before last ad was displayed
                if (nativeAdFB1 == null || nativeAdFB1 !== ad) {
                    return
                }
                // Inflate Native Ad into Container

                // Add the Ad view into the ad container.
                nativeAdLayout = view.findViewById(R.id.native_ad_container_tool_1)
                val inflater = LayoutInflater.from(context)
                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                adView =
                        inflater.inflate(
                                R.layout.native_ad_layout,
                                nativeAdLayout,
                                false
                        ) as LinearLayout
                nativeAdLayout!!.addView(adView)

                inflateAd(nativeAdFB1!!, adView!!)

                val adChoicesContainer: LinearLayout = view.findViewById(R.id.ad_choices_container)
                val adOptionsView = AdOptionsView(context, nativeAdFB1, nativeAdLayout)
                adChoicesContainer.removeAllViews()
                adChoicesContainer.addView(adOptionsView, 0)
            }

            override fun onAdClicked(p0: Ad?) {
                Log.d("TAG", "onAdClicked: onLoadFBNativeAd1")
            }

            override fun onLoggingImpression(p0: Ad?) {
                Log.d("TAG", "onLoggingImpression: onLoadFBNativeAd1")
            }

            override fun onMediaDownloaded(p0: Ad?) {
                Log.d("TAG", "onMediaDownloaded: onLoadFBNativeAd1")
            }
        }

        nativeAdFB1!!.loadAd(
                nativeAdFB1!!.buildLoadAdConfig()
                        .withAdListener(nativeAdListener)
                        .build()
        );
    }

//    fun onLoadFBNativeAd2(view: View,context: Context) {
//        nativeAdFB2 = NativeAd(context, Constants().getFbNativeTool2())
//        val nativeAdListener: NativeAdListener = object : NativeAdListener {
//            override fun onError(p0: Ad?, p1: AdError?) {
//                Log.d("TAG", "onError: onLoadFBNativeAd1 " + p1!!.errorMessage)
//            }
//
//            override fun onAdLoaded(ad: Ad?) {
//
//                // Race condition, load() called again before last ad was displayed
//                if (nativeAdFB2 == null || nativeAdFB2 !== ad) {
//                    return
//                }
//                // Inflate Native Ad into Container
//
//                // Add the Ad view into the ad container.
//                nativeAdLayout = view.findViewById(R.id.native_ad_container_tool_2)
//                val inflater = LayoutInflater.from(context)
//                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
//                adView =
//                        inflater.inflate(
//                                R.layout.native_ad_layout,
//                                nativeAdLayout,
//                                false
//                        ) as LinearLayout
//                nativeAdLayout!!.addView(adView)
//
//                inflateAd(nativeAdFB2!!, adView!!)
//
//                val adChoicesContainer: LinearLayout = view.findViewById(R.id.ad_choices_container)
//                val adOptionsView = AdOptionsView(context, nativeAdFB2, nativeAdLayout)
//                adChoicesContainer.removeAllViews()
//                adChoicesContainer.addView(adOptionsView, 0)
//            }
//
//            override fun onAdClicked(p0: Ad?) {
//                Log.d("TAG", "onAdClicked: onLoadFBNativeAd1")
//            }
//
//            override fun onLoggingImpression(p0: Ad?) {
//                Log.d("TAG", "onLoggingImpression: onLoadFBNativeAd1")
//            }
//
//            override fun onMediaDownloaded(p0: Ad?) {
//                Log.d("TAG", "onMediaDownloaded: onLoadFBNativeAd1")
//            }
//        }
//
//        nativeAdFB2!!.loadAd(
//                nativeAdFB2!!.buildLoadAdConfig()
//                        .withAdListener(nativeAdListener)
//                        .build()
//        );
//    }


    private fun inflateAd(nativeAd: NativeAd, adView: LinearLayout) {
        nativeAd.unregisterView()

        // Add the AdOptionsView

        // Create native UI using the ad metadata.
        val nativeAdIcon: com.facebook.ads.MediaView = adView.findViewById(R.id.native_ad_icon)
        val nativeAdTitle: TextView = adView.findViewById(R.id.native_ad_title)
        val nativeAdMedia: com.facebook.ads.MediaView = adView.findViewById(R.id.native_ad_media)
        val nativeAdSocialContext: TextView = adView.findViewById(R.id.native_ad_social_context)
        val nativeAdBody: TextView = adView.findViewById(R.id.native_ad_body)
        val sponsoredLabel: TextView = adView.findViewById(R.id.native_ad_sponsored_label)
        val nativeAdCallToAction: Button = adView.findViewById(R.id.native_ad_call_to_action)

        // Set the Text.
        nativeAdTitle.text = nativeAd.advertiserName
        nativeAdBody.text = nativeAd.adBodyText
        nativeAdSocialContext.text = nativeAd.adSocialContext
        nativeAdCallToAction.visibility =
                if (nativeAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE
        nativeAdCallToAction.text = nativeAd.adCallToAction
        sponsoredLabel.text = nativeAd.sponsoredTranslation

        // Create a list of clickable views
        val clickableViews: ArrayList<View> = ArrayList()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView, nativeAdMedia, nativeAdIcon, clickableViews
        )
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment InvestFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: Int, param2: String) =
            WebsiteCategoriesFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }


}